from mycodes.Funny_String_utils import funnyString

import unittest

class FunnyStringTest(unittest.TestCase):
    def test_give_acxz_is_Funny(self):
        Funny_String = 'acxz'
        expected_output = 'Funny'

        result = funnyString(Funny_String)

        self.assertEqual(result, expected_output, f'Should be {expected_output}')

    def test_give_abxz_is_Funny(self):
        Funny_String = 'abxz'
        expected_output = 'Not Funny'

        result = funnyString(Funny_String)

        self.assertEqual(result, expected_output, f'Should be {expected_output}')
        