from mycodes.Grid_utils import gridChallenge

import unittest

class GridListTest(unittest.TestCase):
    def test_give_eabcd_fghij_olkmn_trpqs_xywuv_is_grid(self):
        grid = ['eabcd','fghij','olkmn','trpqs','xywuv']
        expected_output = 'YES'

        result = gridChallenge(grid)

        self.assertEqual(result, expected_output, f'should be {expected_output}')

    def test_give_mpxz_abcd_wlmf_is_grid(self):
        grid = ['mpxz','abcd','wlmf']
        expected_output = 'NO'

        result = gridChallenge(grid)

        self.assertEqual(result, expected_output, f'should be {expected_output}')