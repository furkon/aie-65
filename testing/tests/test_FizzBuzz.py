from mycodes.FizzBuzz import fizzbuzz

import unittest

class fizzbuzzTest(unittest.TestCase):
    def test_give_15_is_FizzBuzz(self):
        num = 15
        expected_output = "FizzBuzz"

        result = fizzbuzz(num)

        self.assertEqual(result, expected_output, f"should be {expected_output}")

    def test_give_3_is_FizzBuzz(self):
        num = 3
        expected_output = "Fizz"

        result = fizzbuzz(num)

        self.assertEqual(result, expected_output, f"should be {expected_output}")  

    def test_give_5_is_FizzBuzz(self):
        num = 5
        expected_output = "Buzz"

        result = fizzbuzz(num)

        self.assertEqual(result, expected_output, f"should be {expected_output}")

    def test_give_7_is_FizzBuzz(self):
        num = 7
        expected_output = "Not FizzBuzz"

        result = fizzbuzz(num)

        self.assertEqual(result, expected_output, f"should be {expected_output}")  