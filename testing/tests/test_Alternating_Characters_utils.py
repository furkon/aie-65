from mycodes.Alternating_Characters_utils import alternatingCharacters

import unittest

class AlternatingCharactersStringTest(unittest.TestCase):
    def test_give_a_result_is_number(self):
        alternaing_string = 'AAABBB'
        expected_output = 4

        result = alternatingCharacters(alternaing_string)

        self.assertEqual(result, expected_output,f'should be {expected_output}')
