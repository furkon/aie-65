from mycodes.Two_Characters_utils import alternate

import unittest

class TwoCharactersTest(unittest.TestCase):
    def test_give_beabeefeab_is_two_characters(self):
        string_characters = 'beabeefeab'
        expected_output = 5

        result = alternate(string_characters)

        self.assertEqual(result, expected_output, f'should be {expected_output}')
        