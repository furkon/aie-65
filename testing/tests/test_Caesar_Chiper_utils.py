from mycodes.Caesar_Chiper_utils import caesarCipher

import unittest

class CaeserChiperString(unittest.TestCase):
    def test_give_middleOutz_is_string(self):
        is_string = 'middle-Outz'
        caesarchiper = 2
        expected_output = 'okffng-Qwvb'

        result = caesarCipher(is_string,caesarchiper)

        self.assertEqual(result, expected_output, f'should be {expected_output}')
