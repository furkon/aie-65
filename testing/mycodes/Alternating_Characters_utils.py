def alternatingCharacters(s):
    return len([True for x in range(len(s)-1) if s[x]==s[x+1]])
    