def caesarCipher(s, k):
    text = ''
    for x in s:
        if x.isupper():  
            text += chr((ord(x) + k - 65) % 26 + 65)  
        elif x.islower():  
            text += chr((ord(x) + k - 97) % 26 + 97) 
        else:
            text +=x
            
    return text
    