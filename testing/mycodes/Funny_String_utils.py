def funnyString(s):
    a = [ord(x) for x in s]
    b = [abs(a[x]-a[x+1]) for x in range(len(a)-1)]
    return 'Funny' if b == list(reversed(b)) else 'Not Funny'
    