def gridChallenge(grid):
    new_grid = [sorted(x) for x in grid]
        
    for y in range(len(new_grid[0])):
        for x in range(len(new_grid)-1):
            if new_grid[x][y]<= new_grid[x+1][y]:
                continue
            else:
                return "NO"
        
    return "YES"
    